clc;
clear all;
Nx=10; Ny=30; %Dimension de la maille
L = 1; %dimension de la particule carr� en um
h=0.05; %pas de discr�tisation en um
n = 3; %nombre de nanoparticules
M = zeros(Nx*Ny); %matrice des coefficients.
f = 4/9; %facteur de remplissage
Lx = L/f^(0.5); %Largeur de simulation qui d�pend du facteur de remplissage
Ly = n*Lx+2*h; %Longeur de simulation
E = h^2*-3*eye(Nx*Ny); %matrice diagonale qui contient par d�faut la permittivit� du plastique (3).
%Modification de la matrice E
% for i=1:Nx
%     x = (i-1)*h;
%     for j=1:Ny
%         y = (j-1)*h;
%         if (x==0.5||x==1) && (y==1||y==1.5||y==2.5||y==3||y==4||y==4.5)
%             p = (i-1)*Ny+j;
%             E(p,p) = 3*h^2; %ici je met -9 mais c'est pas la bonne valeur pour l'or.
%         end
%     end
% end

%Modification de la matrice M
for i=1:Nx
    x = (i-1)*h;
    for j=1:Ny
        y = (j-1)*h;
        if (x==0.5||x==1) && (y>=1&&y<=4.5) %tous les points qui n'ont pas besoin de conditions frinti�res sp�ciales.
            p1=(i-1)*Ny+j; M(p1,p1)=-4; %noeud de la grille
            pc=(i-1)*Ny+j-1; M(p1,pc)=1; %champ en haut
            pc=(i-1)*Ny+j+1; M(p1,pc)=1; %champ en bas
            pc=(i-2)*Ny+j; M(p1,pc)=1; %champ � gauche
            pc=(i)*Ny+j; M(p1,pc)=1; %champ � droite
        elseif (x==0.5||x==1) && y==0.5 %conditon sur les points juste apres le 1er PEC et qui ne sont pas dans les conditions p�riodiques.
            p1=(i-1)*Ny+j; M(p1,p1)=-4; %noeud de la grille
            pc=(i-1)*Ny+j+1; M(p1,pc)=1; %champ en bas
            pc=(i-2)*Ny+j; M(p1,pc)=1; %champ � gauche
            pc=(i)*Ny+j; M(p1,pc)=1; %champ � droite
        elseif (x==0.5||x==1) && y==5 %conditon sur les points juste avant le 2e PEC et qui ne sont pas dans les conditions p�riodiques.
            p1=(i-1)*Ny+j; M(p1,p1)=-4; %noeud de la grille
            pc=(i-1)*Ny+j-1; M(p1,pc)=1; %champ en haut
            pc=(i-2)*Ny+j; M(p1,pc)=1; %champ � gauche
            pc=(i)*Ny+j; M(p1,pc)=1; %champ � droite
        elseif (x==0||x==0.5||x==1||x==1.5) && y==0 %condition des noeuds dans le PEC en haut mais pas dans periodique
            p1=(i-1)*Ny+j;
            pc=(i-1)*Ny+j+1; M(p1,pc)=1;
        elseif (x==0||x==0.5||x==1||x==1.5) && y==5.5 %condition des noeuds dans le PEC en bas mais pas dans periodique
            p1=(i-1)*Ny+j;
            pc=(i-1)*Ny+j-1; M(p1,pc)=1;
        elseif x==0 && (y>=1&&y<=4.5) %condition periodique � gauche
            p1=(i-1)*Ny+j; M(p1,p1)=-4;
            pc=(i-1)*Ny+j-1; M(p1,pc)=1; %champ en haut
            pc=(i-1)*Ny+j+1; M(p1,pc)=1; %champ en bas
            pc=(i)*Ny+j; M(p1,pc)=1; %champ � droite
            pc=(Nx-1)*Ny+j; M(p1,pc)=1; %Condition periodique � la gauche
        elseif x==1.5 && (y>=1&&y<=4.5) %condition periodique � droite
            p1=(i-1)*Ny+j; M(p1,p1)=-4;
            pc=(i-1)*Ny+j-1; M(p1,pc)=1; %champ en haut
            pc=(i-1)*Ny+j+1; M(p1,pc)=1; %champ en bas
            pc=(i-2)*Ny+j; M(p1,pc)=1; %champ � gauche
            pc=j; M(p1,pc)=1; %Condition periodique � droite
        elseif x==0 && y==0.5
            p1=(i-1)*Ny+j; M(p1,p1)=-4;
            pc=(i-1)*Ny+j+1; M(p1,pc)=1; %champ en bas
            pc=(i)*Ny+j; M(p1,pc)=1; %champ � droite
        elseif x==0 && y==5
            p1=(i-1)*Ny+j; M(p1,p1)=-4;
            pc=(i)*Ny+j; M(p1,pc)=1; %champ � droite
            pc=(i-1)*Ny+j-1; M(p1,pc)=1; %champ en haut
        elseif x==1.5 && y==0.5
            p1=(i-1)*Ny+j; M(p1,p1)=-4;
            pc=(i-1)*Ny+j+1; M(p1,pc)=1; %champ en bas
            pc=(i-2)*Ny+j; M(p1,pc)=1; %champ � gauche
        elseif x==1.5 && y==5
            p1=(i-1)*Ny+j; M(p1,p1)=-4;
            pc=(i-1)*Ny+j-1; M(p1,pc)=1; %champ en haut
            pc=(i-2)*Ny+j; M(p1,pc)=1; %champ � gauche
        end
    end
end

%Matrice finale pour avoir la forme N*x=lambda*x
N = inv(E)*M;

%D�terminer les valeurs propres de N
Vp = eig(N);
plot(Vp);
